
def fizzBuzz(number: int):
    """
    Function to check if it's a multiple of 3, 5 or both
    :param number: a number whose multiple is to be tested
    :return: "FizzBuzz" if multiple of 3 AND 5, "Buzz" if multiple of 5, "Fizz" if multiple of 3, the number else
    """
    if not number == 0:
        if number % 5 == 0 and number % 3 == 0:
            return "FizzBuzz"
        elif number % 5 == 0:
            return "Buzz"
        elif number % 3 == 0:
            return "Fizz"

    return number
