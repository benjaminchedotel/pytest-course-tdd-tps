import pytest
from fizzBuzz import fizzBuzz


@pytest.mark.parametrize("number", (3, 9, 33))
def test_3_multiple_return_Fizz(number):
    assert fizzBuzz(number) == "Fizz"


@pytest.mark.parametrize("number", (5, 10, 20))
def test_5_multiple_return_Buzz(number):
    assert fizzBuzz(number) == "Buzz"


@pytest.mark.parametrize("number", (15, 30, 75))
def test_5_and_3_multiple_return_FizzBuzz(number):
    assert fizzBuzz(number) == "FizzBuzz"


@pytest.mark.parametrize("number", (0, 1, 78))
def test_not_multiple_3_or_5_return_number(number):
    assert isinstance(number, int)
