def citySearch(search: str):
    """
    Function to search for cities in a list, taking into account the case,
    and being able to find a city with only a part of its name.
    :param search: the string to search in the cities list
    :return: List of cities found, None if search is lesser than 2, all list if search is '*'
    """

    cities_list = ["Paris", "Budapest", "Skopje", "Rotterdam", "Valence", "Vancouver", "Amsterdam", "Vienne", "Sydney",
                   "New York", "Londres", "Bangkok", "Hong Kong", "Dubaï", "Rome", "Istanbul"]

    if search == '*':
        return cities_list

    if len(search) < 2:
        return None

    city_res_list = []
    for city in cities_list:
        if search.lower() in city.lower():
            city_res_list.append(city)

    return city_res_list
