import pytest
from citySearch import citySearch

cities_list = ["Paris", "Budapest", "Skopje", "Rotterdam", "Valence", "Vancouver", "Amsterdam", "Vienne", "Sydney",
               "New York", "Londres", "Bangkok", "Hong Kong", "Dubaï", "Rome", "Istanbul"]


@pytest.mark.parametrize("search, res", [("a", None),
                                         ("ab", []),
                                         ("abc", [])])
def test_less_than_2_char_returns_None(search, res):
    assert citySearch(search) == res


@pytest.mark.parametrize("search, city",
                         [('Ro', ["Rotterdam", "Rome"]),
                          ('Va', ["Valence", "Vancouver"])])
def test_higher_than_2_char_return_city(search, city):
    assert citySearch(search) == city


@pytest.mark.parametrize("search, city",
                         [('VA', ["Valence", "Vancouver"]),
                          ('Va', ["Valence", "Vancouver"]),
                          ('va', ["Valence", "Vancouver"])])
def test_case(search, city):
    assert citySearch(search) == city


@pytest.mark.parametrize("search, city",
                         [('ond', ["Londres"]),
                          ('uba', ["Dubaï"])])
def test_part_of_word(search, city):
    assert citySearch(search) == city


def test_all():
    assert citySearch('*') == cities_list
